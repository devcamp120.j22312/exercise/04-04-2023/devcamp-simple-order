import mobileList from "../assets/data/data";
import { BUY_BUTTON_CLICKED } from "../constants/order.constants";

const initialState = {
    mobileList : [
      {
        name: "IPhone X",
        price: 900,
        quantity: 0
      },
      {
        name: "Samsung S9",
        price: 800,
        quantity: 0
      },
      {
        name: "Nokia 8",
        price: 650,
        quantity: 0
      }
    ]
  }
  

const taskReducers = (state = initialState, action) => {
    switch (action.type) {
        case BUY_BUTTON_CLICKED:
            state.mobileList[action.payload].quantity++;
            break;
    }
    return { ...state }
}

export default taskReducers;
