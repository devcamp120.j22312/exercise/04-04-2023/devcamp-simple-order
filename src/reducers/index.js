// Root reducers (Chứa reducers chính)

import { combineReducers } from 'redux';
import taskReducers from './task.reducers';

const rootReducers = combineReducers ({
    taskReducers
})

export default rootReducers;