import { BUY_BUTTON_CLICKED } from "../constants/order.constants"

export const buyButtonClicked = (id) => {
    return {
        type: BUY_BUTTON_CLICKED,
        payload: id
    }
}
