import { Button, Container, Grid } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import { buyButtonClicked } from '../actions/order.actions'
// import mobileList from "../assets/data/data";

const Order = () => {
    const dispatch = useDispatch();

    const { mobileList } = useSelector((reduxData) => {
        return reduxData.taskReducers
    })

    // Tính tổng giá tiền đơn hàng dựa trên số lượng đã chọn
    const total = mobileList.reduce((summary, item) => summary + item.price * item.quantity, 0);

    const onBtnBuyClickHandler = (id) => {
        dispatch(buyButtonClicked(id))
    }

    return (
        <Container>
            <Grid container mt={5} spacing={3}>
                {mobileList.map((value, index) => {
                    return (
                        <Grid item xs={4} key={index}>
                            <Grid container rowSpacing={1} sx={{ width: 320, heigth: 300, border: "1px solid" }} style={{ display: "flex", alignItems: "center" }}>
                                <Grid style={{marginLeft: "50px", marginBottom: "10px"}}>
                                    <h4>{value.name}</h4>
                                    <p>{value.price}</p>
                                    <p>Quantity: {value.quantity}</p>
                                    <Button style={{backgroundColor: "green", color: "white"}} onClikc={() => onBtnBuyClickHandler(index)}>Buy</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    )
                })}
                
            </Grid>
            <h2>Total: {total} USD</h2>
        </Container>
    )
}

export default Order;